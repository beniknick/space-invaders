//MODULE aliens.js  00 -
//TODO :

import {
  CTX1,
  CANVAS_HEIGHT,
  ALIEN_BULLETS,
  ALL_ALIENS,
} from "../constants.js";

//CREATE ALL ALIENS
//Create 30points alien
// TODO: one30Alien, one20Alien en one10Alien lijken erg veel op elkaar, maak hier daarom 1 class van en geef via de constructor de verschillende
// eigenschappen mee. Als je toch per se verschillende klassen wilt gebruiken, maak dan 1 generieke base class en extend hier van.
export class one30Alien {
  constructor() {
    this.x = 181;
    this.y = 80;
    this.width = 27;
    this.height = 20;
    this.size = 2.5;
    this.isAlive = true;
    this.points = 30;
    this.color = "rgba(255, 0, 0, 1)";
    this.speedX = 50; //Moving Speed
    this.speedY = 50;
    //this.creatAlien();
  }
  draw() {
    //Hit Box
    CTX1.fillStyle = "rgba(00, 0, 255, 0 )";
    CTX1.fillRect(this.x + 1, this.y, this.width, this.height);
    CTX1.fillStyle = "rgba(0, 0, 0, 1)";

    CTX1.fillStyle = this.color;
    //row 1
    CTX1.fillRect(this.x + 8 * this.size, this.y, this.size, this.size);
    CTX1.fillRect(this.x + 3 * this.size, this.y, this.size, this.size);
    //row 2
    // TODO: kan onderstaande, repeterende code, korter? Denk hier over na of dit kan
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    //row 3
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 2 * this.size,
      6 * this.size,
      this.size
    );
    //row 4
    CTX1.fillRect(
      this.x + 2 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 5 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    //row 5
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 4 * this.size,
      10 * this.size,
      this.size
    );
    //row 6
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 5 * this.size,
      6 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    //row 7
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    //row 8
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
  }
}
//Create 20points alien
export class one20Alien {
  constructor() {
    this.x = 181;
    this.y = 100;
    this.width = 27;
    this.height = 20;
    this.size = 2.5;
    this.isAlive = true;
    this.points = 20;
    this.color = "rgba(255, 0, 0, 1)";
    this.speedX = 50; //Moving Speed
    this.speedY = 50;
    //this.creatAlien();
  }
  draw() {
    //Hit Box
    CTX1.fillStyle = "rgba(00, 0, 255, 0 )";
    CTX1.fillRect(this.x + 1, this.y, this.width, this.height);
    CTX1.fillStyle = "rgba(0, 0, 0, 1)";

    CTX1.fillStyle = this.color;
    //row 1
    CTX1.fillRect(this.x + 8 * this.size, this.y, this.size, this.size);
    CTX1.fillRect(this.x + 3 * this.size, this.y, this.size, this.size);
    //row 2
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    //row 3
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 2 * this.size,
      6 * this.size,
      this.size
    );
    //row 4
    CTX1.fillRect(
      this.x + 2 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 5 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    //row 5
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 4 * this.size,
      10 * this.size,
      this.size
    );
    //row 6
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 5 * this.size,
      6 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    //row 7
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    //row 8
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
  }
}
//Create 10points alien
export class one10Alien {
  constructor() {
    this.x = 181;
    this.y = 100;
    this.width = 27;
    this.height = 20;
    this.size = 2.5;
    this.isAlive = true;
    this.points = 10;
    this.color = "rgba(255, 0, 0, 1)";
    this.speedX = 50; //Moving Speed
    this.speedY = 50;
    //this.creatAlien();
  }
  draw() {
    //Hit Box
    CTX1.fillStyle = "rgba(00, 0, 255, 0 )";
    CTX1.fillRect(this.x + 1, this.y, this.width, this.height);
    CTX1.fillStyle = "rgba(0, 0, 0, 1)";

    CTX1.fillStyle = this.color;
    //row 1
    CTX1.fillRect(this.x + 8 * this.size, this.y, this.size, this.size);
    CTX1.fillRect(this.x + 3 * this.size, this.y, this.size, this.size);
    //row 2
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    //row 3
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 2 * this.size,
      6 * this.size,
      this.size
    );
    //row 4
    CTX1.fillRect(
      this.x + 2 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 5 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    //row 5
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 4 * this.size,
      10 * this.size,
      this.size
    );
    //row 6
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 5 * this.size,
      6 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    //row 7
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    //row 8
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
  }
}
//Create alien Ship
// TODO: gebruik lowerCamelCase voor class names
export class alienShip {
  constructor() {
    this.x = 181;
    this.y = 100;
    this.width = 27;
    this.height = 20;
    this.size = 2.5;
    this.isAlive = true;
    this.points = 10;
    this.color = "rgba(255, 0, 0, 1)";
    this.speedX = 50; //Moving Speed
    this.speedY = 50;
    //this.creatAlien();
  }
  draw() {
    //Hit Box
    CTX1.fillStyle = "rgba(00, 0, 255, 0 )";
    CTX1.fillRect(this.x + 1, this.y, this.width, this.height);
    CTX1.fillStyle = "rgba(0, 0, 0, 1)";

    CTX1.fillStyle = this.color;
    //row 1
    CTX1.fillRect(this.x + 8 * this.size, this.y, this.size, this.size);
    CTX1.fillRect(this.x + 3 * this.size, this.y, this.size, this.size);
    //row 2
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + this.size,
      this.size,
      this.size
    );
    //row 3
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 2 * this.size,
      6 * this.size,
      this.size
    );
    //row 4
    CTX1.fillRect(
      this.x + 2 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 5 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 3 * this.size,
      2 * this.size,
      this.size
    );
    //row 5
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 4 * this.size,
      10 * this.size,
      this.size
    );
    //row 6
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 5 * this.size,
      6 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 5 * this.size,
      1 * this.size,
      this.size
    );
    //row 7
    CTX1.fillRect(
      this.x + 1 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 3 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 8 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 10 * this.size,
      this.y + 6 * this.size,
      1 * this.size,
      this.size
    );
    //row 8
    CTX1.fillRect(
      this.x + 4 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
    CTX1.fillRect(
      this.x + 7 * this.size,
      this.y + 7 * this.size,
      1 * this.size,
      this.size
    );
  }
}

//Push set off all one30Aliens
pushAllAliens();
export function pushAllAliens() {
  for (let i = 0; i < 56; i++) {
    ALL_ALIENS.push(new one30Alien());
  }

  //Push 1 set of one10Aliens
  //Push 2 set of one30Aliens
  //Push 2 set of one20Aliens
}

//Setup all aliens to start position
setAllAliens();
export function setAllAliens() {
  let x = ALL_ALIENS[1].x;
  let y = ALL_ALIENS[1].y;
  for (let i = 1; i < ALL_ALIENS.length; i++) {
    ALL_ALIENS[i].x = x;
    ALL_ALIENS[i].y = y;
    x += 40;
    if (i % 11 == 0) {
      x = ALL_ALIENS[1].x;
      y += 50;
    }
  }
}

//Draw all Aliens
export function drawAllAliens(CTX1) {
  for (let i = 1; i < ALL_ALIENS.length; i++) {
    ALL_ALIENS[i].draw(CTX1);
  }
}

//CREATE ALIEN BULLET
export class AlienBullet {
  constructor(random) {
    this.x = ALL_ALIENS[random].x + 14.5;
    this.y = ALL_ALIENS[random].y + 10;
    this.width = ALL_ALIENS[random].isAlive == false ? 0 : 1.3;
    this.height = ALL_ALIENS[random].isAlive == false ? 0 : 8;
    this.size = ALL_ALIENS[random].isAlive == false ? 0 : 5;
    this.speedY = 1; //Moving Speed
  }
  drawBullet() {
    CTX1.fillStyle = "#FF00FF";
    CTX1.fillRect(this.x, this.y, this.width, this.height);
  }
  updateMovement() {
    this.y += this.speedY;
  }
}
export function bulletsFromAlien() {
  //remove Bullets outside canvas
  for (let j = 0; j < ALIEN_BULLETS.length; j++) {
    if (ALIEN_BULLETS[j].y > CANVAS_HEIGHT) {
      ALIEN_BULLETS.splice([j], 1);
    }
  }

  for (let i = 0; i < ALIEN_BULLETS.length; i++) {
    ALIEN_BULLETS[i].updateMovement();
    ALIEN_BULLETS[i].drawBullet(CTX1);
  }
}
