//MODULE levels.js  00 -
//TODO : 

import {CTX1, ALIEN_BULLETS, ALL_ALIENS } from "../constants.js";
import {AlienBullet, pushAllAliens, setAllAliens} from "../modules/aliens.js";
import {SCORE, LIVES} from "../modules/spaceship.js";

//LEVELS VARIABLE
export let HIGH_SCORE = 550;
let levelSpeed = 700;
let alienBulletSpeed = 4000;
let BONUS = 0;
let allAliensDead = false;

function allDeadCheck(){
   let totalDead = 0;
   for (let i = 1; i < ALL_ALIENS.length; i++) {
      if (ALL_ALIENS[i].isAlive == false) {
         totalDead += 1;
         } 
      };
   if ( totalDead == ALL_ALIENS.length -1 )  {
      allAliensDead = true;
   } 
};

//MOVE ALL ALIENS

let xMin= 0;      //Lowest alive aliens x values
let xMax = 0 ;    //Highest alive aliens x values
let alienXMovement = +20 ;     // + right, - left
let alienYMovement = 0 ;      // + down, - up
let movedX = true;
let movedY = true;

   function leftLowNum(){
      let minAlien = ALL_ALIENS.filter( ALL_ALIENS => ALL_ALIENS.isAlive == true);
      let lowNum  = Math.min(...minAlien.map(item => item.x));  // ... Spread Operator makes for a array a row of number.
      xMin = lowNum;       
      };
   function rightHighNum(){
      let maxAlien = ALL_ALIENS.filter( ALL_ALIENS => ALL_ALIENS.isAlive == true);
      let highNum  = Math.max(...maxAlien.map(item => item.x));
      xMax = highNum;
      };
   function moveXAlien(){
      for (let i = 1; i < ALL_ALIENS.length; i++) {
         ALL_ALIENS[i].x += alienXMovement; 
         };

      };
   function moveYAlien(){
      for (let j = 1; j < ALL_ALIENS.length; j++) {
         alienYMovement = +20;
            ALL_ALIENS[j].x += alienXMovement; 
            ALL_ALIENS[j].y += alienYMovement; 
         alienYMovement = 0; 
         };
     
      };
      
 
//ALIENS ANIMATION INTERVAL
setInterval(function(){
   //Move aliens
   leftLowNum();        //console.log('X min = ' + xMin);
   rightHighNum();      //console.log('X max = ' + xMax);
      if( xMax == 701 && movedX == true){ 
         moveYAlien();
         alienXMovement = -20;

         movedX = false;
         movedY = true;
         } ;
      if( xMin == 61 && movedY == true){ 
         moveYAlien();
         alienXMovement = +20;
         
         movedY = false;
         movedX = true;
         };
   moveXAlien(); 

   //New set off aliens
   allDeadCheck();  
      if ( allAliensDead == true){
         //Remove all aliens from array
         for (let i = ALL_ALIENS.length - 1;  i >= 0;  i -= 1)  {
            ALL_ALIENS.splice([i], 1);
            };
         //New aliens   
         pushAllAliens();
         setAllAliens();
         
         allAliensDead = false;
      };
   
}, levelSpeed);

//RANDOM ALIEN BULLET INTERVAL
setInterval(function(){
   let random = Math.floor(Math.random() * 55 + 1)
      for (let i = 0; i < 56; i++) {
         if (ALL_ALIENS[random].isAlive == false){
            random = Math.floor(Math.random() * 55 + 1)
         }else { ALIEN_BULLETS.push(new AlienBullet(random));
      break; };
      };      
   }, alienBulletSpeed);


//RANDOM BONUS 
//  
//NEW SET OFF ALIENS + SPEED INCREASE








//HIGH SCORE
export function DrawHighScore(){
   CTX1.font = "15px Arial";
   CTX1.fillStyle = "white";
   CTX1.fillText('HIGH SCORE \xa0' + HIGH_SCORE,20,30);
 };
 //SCORE
export function DrawScore(){
   CTX1.font = "20px Arial";
   CTX1.fillStyle = "white";
   CTX1.fillText('SCORE \xa0' + SCORE ,350,30);
 };
 //LIVES
export function DrawLives(){
   CTX1.font = "20px Arial";
   CTX1.fillStyle = "green";
   CTX1.fillText('LIVES \xa0' + LIVES.toFixed(0),640,30);
 };
 