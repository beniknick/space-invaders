//MODULE spaceShip.js  02 -
//TODO :

import {
  CANVAS_WIDTH,
  CTX1,
  SHIP_BULLETS,
  ALIEN_BULLETS,
  ALL_ALIENS,
} from "../constants.js";

export let SCORE = 0;
export let LIVES = 3;
//CREATE SPACESHIP
export class SpaceShip {
  // TODO: geef alle start waarden (x,y, etc.) mee via constructor zodat je deze class later ook voor andere settings kunt gebruiken
  constructor() {
    this.x = 400;
    this.y = 570;
    this.width = 22; //Hit box
    this.height = 40; //Hit box
    this.speed = 15; //Moving Speed
    this.isShipAlive = true;
    this.ship = "\u{1F680}";
    this.shipColor = "rgba(0, 0, 0, 1)";
    //SPACESHIP CONTROLS INPUTS
    window.addEventListener("keydown", (event) => {
      // TODO: 1 swich statement van 3 ifs maken, is leesbaarder
      if (event.key == "ArrowLeft") {
        if (this.x <= 60) {
          this.x = 60;
        } else {
          this.x -= this.speed;
        }
      }

      if (event.key == "ArrowRight") {
        if (this.x >= CANVAS_WIDTH - 80) {
          this.x = CANVAS_WIDTH - 80;
        } else {
          this.x += this.speed;
        }
      }

      if (event.key == " ") {
        if (this.isShipAlive == false) {
        } else {
          SHIP_BULLETS.push(new ShipBullet(this.x, this.y));
        }
      }
      //console.log(event.key)
    });
  }
  //DRAW SPACESHIP
  draw(CTX1) {
    //Hit Box
    CTX1.fillStyle = "rgba(255, 0, 0, 0)";
    CTX1.fillRect(this.x - 2, this.y - 40, this.width, this.height);
    CTX1.fillStyle = this.shipColor;

    CTX1.translate(this.x, this.y);
    CTX1.font = "27px Arial";
    CTX1.rotate((-45 * Math.PI) / 180);
    CTX1.fillText(this.ship, 0, +4);
    CTX1.rotate((45 * Math.PI) / 180);
    CTX1.translate(-this.x, -this.y);
  }
  //Draw all bullets
  // TODO: kies een duidelijkere method name, bijv.: drawBullets
  bulletsFromShip(CTX1) {
    //remove Bullets outside canvas
    // TODO: onderstaande loops kunnen vervangen worden voor 1 loop, is efficienter
    for (let j = 0; j < SHIP_BULLETS.length; j++) {
      if (SHIP_BULLETS[j].y < 0) {
        SHIP_BULLETS.splice([j], 1);
      }
    }

    for (let i = 0; i < SHIP_BULLETS.length; i++) {
      SHIP_BULLETS[i].updateMovement();
      SHIP_BULLETS[i].drawBullet(CTX1);
      this.AliensHit();
    }
  }
  //Hit ship detection
  hitShip() {
    for (let i = 0; i < ALIEN_BULLETS.length; i++) {
      // TODO: waarom een nieuw object aanmaken? Is overbodig, gebruik in deze method this ipv nieuw object
      const ship = {
        x: this.x,
        y: this.y,
        width: this.width,
        height: this.height,
      };
      const alien = ALIEN_BULLETS[i];

      if (
        ship.x > alien.x + alien.width ||
        ship.x + ship.width < alien.x ||
        ship.y > alien.y + alien.height ||
        ship.y + ship.h < alien.y
      ) {
      } else {
        //Remove live
        // TODO: gebruik hele punten voor lives om problemen met afronding etc. te voorkomen
        LIVES -= 0.025; // 1/40 = 1 punt
        Math.round(LIVES);

        console.log("HIT SHIP");

        //Remove SpaceShip
        if (LIVES <= 0) {
          this.isShipAlive = false;
          this.shipColor = "rgba(0, 0, 0, 0)";
        }
        break;
      }
    }
  }
  //ALIENS HIT DETECTION
  // TODO: gebruik lowerPascalCase voor alle variablen en method names (consitentie voor betere leesbaarheid)
  AliensHit() {
    // TODO: gebruik voor geneste loops eerst i, daanra j (en niet andersom), voor consistentie en leesbaarheid
    // TODO: idee: er is gelijkheid in functionaliteit tussen
    for (let j = 0; j < SHIP_BULLETS.length; j++) {
      for (let i = ALL_ALIENS.length - 1; i >= 0; i -= 1) {
        if (
          SHIP_BULLETS[j].x < ALL_ALIENS[i].x + ALL_ALIENS[i].width &&
          SHIP_BULLETS[j].x + SHIP_BULLETS[j].width > ALL_ALIENS[i].x &&
          SHIP_BULLETS[j].y < ALL_ALIENS[i].y + ALL_ALIENS[i].height &&
          SHIP_BULLETS[j].y + SHIP_BULLETS[j].height > ALL_ALIENS[i].y &&
          ALL_ALIENS[i].isAlive === true
        ) {
          //Remove alien
          ALL_ALIENS[i].isAlive = false;
          ALL_ALIENS[i].color = "rgba(0, 0, 0, 0)";
          //Add score
          SCORE += ALL_ALIENS[i].points;
          //Remove bullet
          SHIP_BULLETS.splice([j], 1);
          break;
        } else {
        }

        console.log();
      }
    }
  }
}

// TODO: zet elke class in een eigen file voor betere project structuur

//CREATE SPACESHIP BULLET
class ShipBullet {
  // TODO: gerbuik nog iets duidelijkere namen (is al vrij goed), xPos en yPos is nog iets duidelijker dan x en y
  constructor(x, y) {
    this.x = x + 5;
    this.y = y - 40;
    this.width = 1.3;
    this.height = 8;
    this.size = 5;
    this.speedY = -2.8;
  }
  // TODO: gebruik een generieke naam voor het tekenen van alle objecten, dus als je draw() voor Spaceship gebruikt, doe dat dan ook voor alle
  // andere classes (consitentie zorg voor minder fouten)
  drawBullet() {
    CTX1.fillStyle = "#00FFFF";
    CTX1.fillRect(this.x, this.y, this.width, this.height);
  }
  updateMovement() {
    this.y += this.speedY;
  }
}

//INFO
//&#x1F680; =rocket ship unicode
//&#x1F6E6; =Up-Pointing Military Airplane
