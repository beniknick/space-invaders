// TODO: installeer code formatter zoals Prettier (plugin VS studio code) voor automatische code opmaak

//SPACE INVADERS THE GAME  02 -

import { CTX1, CANVAS_HEIGHT, CANVAS_WIDTH } from "./constants.js";
import { SpaceShip } from "./modules/spaceship.js";
import { bulletsFromAlien, drawAllAliens } from "./modules/aliens.js";
import { DrawHighScore, DrawScore, DrawLives } from "./modules/levels.js";

//ONE SPACESHIP
export let spaceShip = new SpaceShip();
window.spaceShip = spaceShip;

//STARS BACKGROUND
const stars = [];

//update drawings for Animate
function updateCanvas() {
  CTX1.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
  drawStars();
  DrawHighScore();
  DrawScore();
  DrawLives();

  drawAllAliens();
  bulletsFromAlien();

  spaceShip.hitShip(); //40x per sec
  spaceShip.draw(CTX1);
  spaceShip.bulletsFromShip(CTX1);

  requestAnimationFrame(updateCanvas); //makes a loop on updateCanvas (animate) if called
}
updateCanvas();

//RANDOM STARS BACKGROUND
class Star {
  constructor() {
    this.x = Math.random() * CANVAS_WIDTH;
    this.y = Math.random() * CANVAS_HEIGHT;
    this.size = Math.random() * 2;
  }
  draw() {
    CTX1.fillStyle = "white";
    CTX1.beginPath();
    CTX1.arc(this.x, this.y, this.size, 0, Math.PI * 2);
    CTX1.fill();
    //ctx1.closePath();
  }
}
for (let i = 0; i < 175; i++) {
  stars.push(new Star());
}
function drawStars() {
  for (let i = 0; i < stars.length; i++) {
    stars[i].draw();
  }
}
