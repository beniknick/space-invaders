//CANVAS VARIABLE
export const CANVAS = document.getElementById("canvas1");
export const CTX1 = CANVAS.getContext("2d");
export const CANVAS_WIDTH = 800;
export const CANVAS_HEIGHT = 600;
CANVAS.width = CANVAS_WIDTH;
CANVAS.height = CANVAS_HEIGHT;
CANVAS.style.backgroundColor = "rgb(15, 15, 15)";

//GAME VARIABLE

export const SHIP_BULLETS = [];
export const ALL_ALIENS = [];
export const ALIEN_BULLETS = [];
