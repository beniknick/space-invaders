//SPACE INVADERS THE GAME  02 -
//TODO :

const canvas = document.getElementById("canvas1");
const ctx1 = canvas.getContext("2d");
ctx1.canvas.width = 1200;
ctx1.canvas.height = 400;
const shipBullets = [];

window.addEventListener("resize", function () {
  ctx1.canvas.width = 1200;
  ctx1.canvas.height = 400;
});

const mouse = {
  x: null,
  y: 300,
};
//one new Circle on click
window.addEventListener("keydown", function (event) {
  if (event.key == " ") {
    shipBullets.push(new Bullet());
  }

  console.log(event.key);
});

//drawCircle follows mouse movement
canvas.addEventListener("mousemove", function (event) {
  mouse.x = event.x;
});

class Bullet {
  constructor() {
    this.x = mouse.x;
    this.y = mouse.y;
    // this.x = Math.random() * canvas.width;
    // this.y = Math.random() * canvas.height;
    this.size = 5;
    // this.size = Math.random() * 20 + 1;
    this.speedX = Math.random() * 3 - 1.5;
    this.speedY = -3;
  }
  drawBullet() {
    ctx1.fillStyle = "blue";
    ctx1.beginPath();
    ctx1.arc(this.x, this.y, this.size, 0, Math.PI * 2);
    ctx1.fill();
    //ctx1.closePath();
  }
  updateMovement() {
    //this.x += this.speedX;
    this.y += this.speedY;
    //if (this.size > 0.2) this.size -= 0.1; //Circle from Big to small size
    // if( this.x + this.width < 0) {this.x = canvas.width} //links uitbeeld, rechts inbeeld
  }
}

function bulletsFromShip() {
  for (let i = 0; i < shipBullets.length; i++) {
    shipBullets[i].updateMovement();
    shipBullets[i].drawBullet();
  }
}

function animate() {
  ctx1.clearRect(0, 0, canvas.width, canvas.height);
  bulletsFromShip();
  requestAnimationFrame(animate); //makes a loop op animate if called
}
animate();

// TODO: ongebruikte code verwijderen

/* draw test 3 - draw 100 moving circles

window.addEventListener('click', function(){
  const canvas = document.getElementById("canvas1");
  const ctx1 = canvas.getContext("2d");
  ctx1.canvas.width  = 1200;
  ctx1.canvas.height = 400;
  
  let particleArray = [];

  // window.addEventListener('resize', function(){
  //   ctx1.canvas.width  = 500;
  //   ctx1.canvas.height = 500;
   
  // })
  
  // CREATE PARTICLE OBJECT ///////////////////
  class Particle {
      constructor(){
          this.x = Math.random() * canvas.width;
          this.y = Math.random() * canvas.height;
          this.size = 5;
          // this.speedX = Math.random() * 3 - 1.5;
          // this.speedY = Math.random() * 3 - 1.5;        
      }
    draw(){
      ctx1.fillStyle = 'blue'
      ctx1.beginPath();
      ctx1.arc(this.x,this.y,this.size,0,Math.PI * 2,);
      ctx1.fill();
      //ctx1.closePath();
    }
    // update(){
    //     this.x += this.speedX;
    //     this.y += this.speedY;
    // }
  }
  
  function init() {
      particleArray = [];
      for (let i = 0; i < 100; i++){
          particleArray.push(new Particle());
      }
  
  }
  init();

  function animate(){
      ctx1.clearRect(0, 0, canvas.width, canvas.height);
      for (let i = 0; i < particleArray.length; i++) {
          //particleArray[i].update();
          particleArray[i].draw();
      }
     
      //requestAnimationFrame(animate);
  }

  animate();

/* move circle with keyboard test

const canvas = document.getElementById("canvas1");
const ctx1 = canvas.getContext("2d");
ctx1.canvas.width  = 600;
ctx1.canvas.height = 400;
const particleArray = [];

window.addEventListener('resize', function(){
  ctx1.canvas.width  = 600;
  ctx1.canvas.height = 400;
})

const mouse = {
  x: 300,
  y: 320,
}

window.addEventListener('keydown', function (event) {
  if (event.key == 'ArrowLeft') { mouse.x -= 25; animate()}
  if (event.key == 'ArrowRight') { mouse.x += 25; animate()}
  console.log(event.key)
})
window.addEventListener('keyup', function (event) {

})

function draw(){
  ctx1.fillStyle = 'blue'
  ctx1.beginPath();
  ctx1.arc(mouse.x,mouse.y,50,0,Math.PI * 2,);
  ctx1.fill();
};

function animate(){  
  ctx1.clearRect(0,0, canvas.width, canvas.height);
  draw();
  //setInterval(animate, 20);
  //requestAnimationFrame(animate); //makes a loop op animate if called
};

draw();

*/

/* hit detected function test

//Rectangle hit detected function
function hit(){  
  const rect1 = { x:5, y:5, width: 50, height:50 };
  const rect2 = { x:10, y:15, width: 40, height:50 };

  if ( rect1.x > rect2.x + rect2.width ||
       rect1.x + rect1.width < rect2.x ||
       rect1.y > rect2.y + rect2.height ||
       rect1.y + rect1.height < rect2.y
    ){   // no hit
    } else {
      //hit detected
    }
};

//Circle hit detected function
function hit(){  
  const circle1 = { x:5, y:5, radius: 50 };
  const circle2 = { x:5, y:5, radius: 50 };

  let dx = circle2.x - circle1.x;
  let dy = circle2.y - circle1.y;
  let distance = Math.sqrt(dx * dx + dy * dy);
  let sumOfRadius = circle1.radius + circle2.radius;

    if ( distance < sumOfRadius){
      // hit
    } else if ( distance === sumOfRadius){
      // circles are touching
    } else if ( distance > sumOfRadius){
      // no hit
    };

};

*/

/* draw test 3 - draw 10 random moving circles on click
const canvas = document.getElementById("canvas1");
const ctx1 = canvas.getContext("2d");
ctx1.canvas.width  = 1200;
ctx1.canvas.height = 400;
const particleArray = [];

window.addEventListener('resize', function(){
  ctx1.canvas.width  = 1200;
  ctx1.canvas.height = 400;
})

const mouse = {
  x: null,
  y: null,
}
//one new Circle on click
canvas.addEventListener('click', function(event){
    mouse.x = event.x;
    mouse.y = event.y;
    for (let i = 0; i < 10; i++) {
      particleArray.push(new Particle());
    };
    
    //console.log(mouse);
});
//drawCircle follows mouse movement
 canvas.addEventListener('mousemove', function(event){
      mouse.x = event.x;
      mouse.y = event.y;
    })

class Particle {
  constructor(){
      this.x = mouse.x;
      this.y = mouse.y;
      // this.x = Math.random() * canvas.width;
      // this.y = Math.random() * canvas.height;
      this.size = 5;
      // this.size = Math.random() * 20 + 1; 
      this.speedX = Math.random() * 3 - 1.5;
      this.speedY = Math.random() * 3 - 1.5;        
  }
  draw(){
    ctx1.fillStyle = 'blue'
    ctx1.beginPath();
    ctx1.arc(this.x,this.y,this.size,0,Math.PI * 2,);
    ctx1.fill();
    //ctx1.closePath();
  }
  update(){
      this.x += this.speedX;
      this.y += this.speedY;
      //if (this.size > 0.2) this.size -= 0.1; //Circle from Big to small size
      // if( this.x + this.width < 0) {this.x = canvas.width} //links uitbeeld, rechts inbeeld
    }
};

function handel(){
  for (let i = 0; i < particleArray.length; i++){
  particleArray[i].update();
  particleArray[i].draw();
  }
};

function animate(){  
  ctx1.clearRect(0,0, canvas.width, canvas.height);
  handel()
  requestAnimationFrame(animate); //makes a loop op animate if called
};
animate(); 

*/

/* draw test 3 - draw 100 moving circles

window.addEventListener('click', function(){
  const canvas = document.getElementById("canvas1");
  const ctx1 = canvas.getContext("2d");
  ctx1.canvas.width  = 1200;
  ctx1.canvas.height = 400;
  
  let particleArray = [];

  // window.addEventListener('resize', function(){
  //   ctx1.canvas.width  = 500;
  //   ctx1.canvas.height = 500;
   
  // })
  
  // CREATE PARTICLE OBJECT ///////////////////
  class Particle {
      constructor(){
          this.x = Math.random() * canvas.width;
          this.y = Math.random() * canvas.height;
          this.size = 5;
          // this.speedX = Math.random() * 3 - 1.5;
          // this.speedY = Math.random() * 3 - 1.5;        
      }
    draw(){
      ctx1.fillStyle = 'blue'
      ctx1.beginPath();
      ctx1.arc(this.x,this.y,this.size,0,Math.PI * 2,);
      ctx1.fill();
      //ctx1.closePath();
    }
    // update(){
    //     this.x += this.speedX;
    //     this.y += this.speedY;
    // }
  }
  
  function init() {
      particleArray = [];
      for (let i = 0; i < 100; i++){
          particleArray.push(new Particle());
      }
  
  }
  init();

  function animate(){
      ctx1.clearRect(0, 0, canvas.width, canvas.height);
      for (let i = 0; i < particleArray.length; i++) {
          //particleArray[i].update();
          particleArray[i].draw();
      }
     
      //requestAnimationFrame(animate);
  }

  animate();
  
 

});



*/

/* draw test 2 - draw moving circles 01
const canvas = document.getElementById("canvas1");
const ctx1 = canvas.getContext("2d");
ctx1.canvas.width  = 1200;
ctx1.canvas.height = 400;
const particleArray = [];

window.addEventListener('resize', function(){
  ctx1.canvas.width  = 1200;
  ctx1.canvas.height = 400;
})

const mouse = {
  x: null,
  y: null,
}
//one new Circle on click
canvas.addEventListener('click', function(event){
    mouse.x = event.x;
    mouse.y = event.y;
    drawCircle();
    //console.log(mouse);
});
//drawCircle follows mouse movement
 canvas.addEventListener('mousemove', function(event){
      mouse.x = event.x;
      mouse.y = event.y;
      drawCircle();
    })

// function drawCircle(){
//   ctx1.fillStyle = 'blue'
//   ctx1.beginPath();
//   ctx1.arc(mouse.x,mouse.y,20,0,Math.PI * 2,);
//   ctx1.fill();
// };

class Particle {
  constructor(){
      // this.x = mouse.x;
      // this.y = mouse.y;
      this.x = Math.random() * canvas.width;
      this.y = Math.random() * canvas.height;
      this.size = 5;
      this.speedX = Math.random() * 3 - 1.5;
      this.speedY = Math.random() * 3 - 1.5;        
  }
  draw(){
    ctx1.fillStyle = 'blue'
    ctx1.beginPath();
    ctx1.arc(this.x,this.y,20,0,Math.PI * 2,);
    ctx1.fill();
    //ctx1.closePath();
  }
  update(){
      this.x += this.speedX;
      this.y += this.speedY;
  }
};

function init() {
  for (let i = 0; i < 100; i++){
      particleArray.push(new Particle());
  }
};
init();

function handel(){
  for (let i = 0; i < particleArray.length; i++){
  particleArray[i].update();
  particleArray[i].draw();
  }
};

function animate(){  
  ctx1.clearRect(0,0, canvas.width, canvas.height);
  handel()
  requestAnimationFrame(animate); //makes a loop op animate if called
};
animate(); 
*/

/* draw test 1 - draw one circle on mouse click or paint 
const canvas = document.getElementById("canvas1");
const ctx1 = canvas.getContext("2d");
ctx1.canvas.width  = 1200;
ctx1.canvas.height = 400;

window.addEventListener('resize', function(){
  ctx1.canvas.width  = 1200;
  ctx1.canvas.height = 400;
})

const mouse = {
  x: null,
  y: null,
}
//one new Circle on click
canvas.addEventListener('click', function(event){
    mouse.x = event.x;
    mouse.y = event.y;
    drawCircle();
    //console.log(mouse);
});
//drawCircle follows mouse movement
//  canvas.addEventListener('mousemove', function(event){
//       mouse.x = event.x;
//       mouse.y = event.y;
//       drawCircle();
//     })

function drawCircle(){
  ctx1.fillStyle = 'blue'
  ctx1.beginPath();
  ctx1.arc(mouse.x,mouse.y,20,0,Math.PI * 2,);
  ctx1.fill();
};

function animate(){  
  ctx1.clearRect(0,0, canvas.width, canvas.height);
  drawCircle();
  requestAnimationFrame(animate); //makes a loop op animate if called
};
animate(); 

*/
